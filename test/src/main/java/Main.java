import com.github.chen0040.fpm.AssocRuleMiner;
import com.github.chen0040.fpm.apriori.Apriori;
import com.github.chen0040.fpm.data.ItemSets;
import com.github.chen0040.fpm.data.MetaData;
import com.github.chen0040.fpm.fpg.FPGrowth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<List<String>> database = new ArrayList<>();

// each line below add a new transaction to the database
        database.add(Arrays.asList("f", "a", "c", "d", "g", "i", "m", "p"));
        database.add(Arrays.asList("a", "b", "c", "f", "l", "m", "o"));
        database.add(Arrays.asList("b", "f", "h", "j", "o", "w"));
        database.add(Arrays.asList("b", "c", "k", "s", "p"));
        database.add(Arrays.asList("a", "f", "c", "e", "l", "p", "m", "n"));

        AssocRuleMiner method = new FPGrowth();
        method.setMinSupportLevel(2);

        MetaData metaData = new MetaData(database);

// obtain all frequent item sets with support level not below 2
        ItemSets frequent_item_sets = method.minePatterns(database, metaData.getUniqueItems());
        frequent_item_sets.stream().forEach(itemSet -> System.out.println("item-set: " + itemSet));

// obtain the max frequent item sets
        ItemSets max_frequent_item_sets = method.findMaxPatterns(database, metaData.getUniqueItems());
        System.out.println(max_frequent_item_sets.getSets());

    }
}
